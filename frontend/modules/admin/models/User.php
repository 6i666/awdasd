<?php
namespace frontend\modules\admin\models;

use yii\helpers\ArrayHelper;
use Yii;
use frontend\modules\admin\Module;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $email_confirm_token
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $password_hash
 * @property string $password_reset_token
 */
class User extends \frontend\modules\user\models\User
{
    const SCENARIO_ADMIN_CREATE = 'adminCreate';
    const SCENARIO_ADMIN_UPDATE = 'adminUpdate';

    public $newPassword;
    public $newPasswordRepeat;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['newPassword', 'newPasswordRepeat'], 'required', 'on' => self::SCENARIO_ADMIN_CREATE],
            ['newPassword', 'string', 'min' => 6],
            ['newPasswordRepeat', 'compare', 'compareAttribute' => 'newPassword'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_ADMIN_CREATE => ['username', 'email', 'status', 'role', 'newPassword', 'newPasswordRepeat'],
            self::SCENARIO_ADMIN_UPDATE => ['username', 'email', 'status', 'role', 'newPassword', 'newPasswordRepeat'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'newPassword' => Module::t('module', 'USER_NEW_PASSWORD'),
            'newPasswordRepeat' => Module::t('module', 'USER_REPEAT_PASSWORD'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!empty($this->newPassword)) {
                $this->setPassword($this->newPassword);
            }

            return true;
        }

        return false;
    }
}