<?php

use yii\helpers\Html;
use frontend\modules\admin\Module;


/* @var $this yii\web\View */
/* @var $model frontend\modules\user\models\User */

$this->title = Module::t('module', 'ADMIN_CREATE_USER');
$this->params['breadcrumbs'][] = ['label' => Module::t('module', 'ADMIN_USERS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
