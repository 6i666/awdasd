<?php
return [
    'ADMIN' => 'Админка',
    'ADMIN_USERS' => 'Пользователи',
    'ADMIN_CREATE_USER' => 'Создать пользователя',
    'ADMIN_UPDATE_USER' => 'Обновить пользователя: {username}',
    'ADMIN_DELETE_CONFIRM' => 'Вы уверены что хотите удалить пользователя?',
    'NAV_ADMIN' => 'Админка',

    'USER_NEW_PASSWORD' => 'Новый пароль',
    'USER_REPEAT_PASSWORD' => 'Повторите новый пароль',

    'ADMIN_CREATE' => 'Создать',
    'ADMIN_UPDATE' => 'Обновить',
    'ADMIN_SEARCH' => 'Искать',
    'ADMIN_RESET' => 'Сбросить',
    'ADMIN_DELETE' => 'Удалить',
];