<?php
return [
    'ADMIN' => 'Administation',
    'ADMIN_USERS' => 'Users',
    'ADMIN_CREATE_USER' => 'Create user',
    'ADMIN_UPDATE_USER' => 'Update user: {username}',
    'ADMIN_DELETE_CONFIRM' => 'Are you sure you want to delete this item?',
    'NAV_ADMIN' => 'Administation',

    'USER_NEW_PASSWORD' => 'New password',
    'USER_REPEAT_PASSWORD' => 'Repeat new password',

    'ADMIN_CREATE' => 'Create',
    'ADMIN_UPDATE' => 'Update',
    'ADMIN_SEARCH' => 'Search',
    'ADMIN_RESET' => 'Reset',
    'ADMIN_DELETE' => 'Delete',
];