<?php

namespace frontend\modules\user;

use yii\base\BootstrapInterface;
use Yii;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/user/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@frontend/modules/user/messages',
            'fileMap' => [
                'modules/user/module' => 'module.php',
                'modules/user/mails' => 'mails.php',
            ],
        ];
    }
}