<?php
use yii\helpers\Html;
use frontend\modules\user\Module;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/password-reset', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p><?= Module::t('mails', 'HELLO {username}', ['username' => $user->username]) ?></p>

    <p><?= Module::t('mails', 'FOLLOW_TO_RESET_PASSWORD') ?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
