<?php

namespace frontend\modules\user\views;

use Yii;
use yii\helpers\Html;
use frontend\modules\user\Module;

/* @var $this yii\web\View */
/* @var $user frontend\modules\user\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/default/email-confirm', 'token' => $user->email_confirm_token]);
?>

<?= Module::t('mails', 'HELLO {username}', ['username' => $user->username]); ?>

<?= Module::t('mails', 'FOLLOW_TO_CONFIRM_EMAIL') ?>

<?= Html::a($confirmLink, $confirmLink) ?>

<?= Module::t('mails', 'IGNORE_IF_DO_NOT_REGISTER') ?>
