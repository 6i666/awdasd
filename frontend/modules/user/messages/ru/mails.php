<?php
return [
    // Mails.
    'HELLO {username}' => 'Здравствуйте, {username}!',
    'FOLLOW_TO_RESET_PASSWORD' => 'Для смены пароля пройдите по ссылке:',
    'FOLLOW_TO_CONFIRM_EMAIL' => 'Для подтверждения адреса пройдите по ссылке:',
    'IGNORE_IF_DO_NOT_REGISTER' => 'Если Вы не регистрировались на нашем сайте, то просто удалите это письмо.',
];