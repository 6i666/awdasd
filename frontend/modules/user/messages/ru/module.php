<?php
return [
    // Profile.
    'TITLE_PASSWORD_CHANGE' => 'Смена пароля',
    'TITLE_PROFILE' => 'Профиль',

    // Bttons.
    'LINK_PASSWORD_CHANGE' => 'Изменить пароль',
    'USER_CURRENT_PASSWORD' => 'Текущий пароль',
    'USER_NEW_PASSWORD' => 'Новый пароль',
    'USER_REPEAT_PASSWORD' => 'Повторите новый пароль',
    'BUTTON_UPDATE' => 'Обновить',
    'BUTTON_SAVE' => 'Сохранить',
    'BUTTON_SIGNUP' => 'Зарегистрироваться',
    'BUTTON_SEND' => 'Отправить',
    'BUTTON_DELETE' => 'Удалить',
    'BUTTON_LOGIN' => 'Войти',

    // Login form.
    'USER_USERNAME' => 'Логин',
    'USER_PASSWORD' => 'Пароль',
    'USER_REMEMBER_ME' => 'Запомнить меня',

    // Reset passwoed page.
    'RESET_PASSWORD_PAGE_TITLE' => 'Сброс пароля',
    'TEXT_RESET_PASSWORD' => 'Введите свой email. Вам на почту прийдет письмо со ссылкой.',
    'RESET_PASSWORD_PAGE_TEXT' => 'Введите новый пароль:',

    // Login page.
    'LOGIN_PAGE_TITLE' => 'Войти',
    'LOGIN_PAGE_TEXT' => 'Для входа на сайт введите данные своей учетной записи:',
    'RESET_PASSWORD_TEXT' => 'Зыбыли пароль?',
    'RESET_PASSWORD_LINK_TEXT' => 'восстановить',

    // Signup page.
    'SIGNUP_PAGE_TITLE' => 'Регистрация',
    'SIGNUP_PAGE_TEXT' => 'Пожалуйста заполните следцющие поля чтобы зарегистрироваться:',

    // Profile.
    'TITLE_PROFILE' => 'Профиль',

    'FORM_CAPTHA' => 'Введите код с картинки',

    // Errors.
    'ERROR_USERNAME_EXISTS' => 'Этот ник уже занят.',
    'ERROR_EMAIL_EXISTS' => 'Пользователь с таким email уже существует.',
    'ERROR_WRONG_CURRENT_PASSWORD' => 'Неверный текущий пароль',
    'ERROR_WRONG_USERNAME_OR_PASSWORD' => 'Неверное имя пользователя или пароль.',
    'ERROR_PROFILE_BLOCKED' => 'Ваш аккаунт заблокирован.',
    'ERROR_PROFILE_NOT_ACTIVATE' => 'Ваш аккаунт не подтвежден.',
    'ERROR_USER_NOT_FOUND_BY_EMAIL' => 'Пользователь с таким email не найден.',

    'USER_ROLE' => 'Роль',
];