<?php
return [
    // Mails.
    'HELLO {username}' => 'Hello, {username}!',
    'FOLLOW_TO_RESET_PASSWORD' => 'Follow the link below to reset your password:',
    'FOLLOW_TO_CONFIRM_EMAIL' => 'Follow the link below to confirm your email:',
    'IGNORE_IF_DO_NOT_REGISTER' => 'If you do not register on our site just remove this mail.',
];