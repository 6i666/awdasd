<?php
return [
    // Profile.
    'TITLE_PASSWORD_CHANGE' => 'Change password',
    'TITLE_PROFILE' => 'Profile',

    // Bttons.
    'LINK_PASSWORD_CHANGE' => 'Change your password',
    'USER_CURRENT_PASSWORD' => 'Current password',
    'USER_NEW_PASSWORD' => 'New password',
    'USER_REPEAT_PASSWORD' => 'Repeat new password',
    'BUTTON_UPDATE' => 'Update',
    'BUTTON_SAVE' => 'Save',
    'BUTTON_SIGNUP' => 'Signup',
    'BUTTON_SEND' => 'Send',
    'BUTTON_DELETE' => 'Delete',
    'BUTTON_LOGIN' => 'Login',

    // Login form.
    'USER_USERNAME' => 'Login',
    'USER_PASSWORD' => 'Password',
    'USER_REMEMBER_ME' => 'Remember me',

    // Reset passwoed page.
    'RESET_PASSWORD_PAGE_TITLE' => 'Reset password',
    'TEXT_RESET_PASSWORD' => 'Please fill out your email. A link to reset password will be sent there.',
    'RESET_PASSWORD_PAGE_TEXT' => 'Please choose your new password:',

    // Login page.
    'LOGIN_PAGE_TITLE' => 'Login',
    'LOGIN_PAGE_TEXT' => 'Please fill out the following fields to login:',
    'RESET_PASSWORD_TEXT' => 'If you forgot your password you can',
    'RESET_PASSWORD_LINK_TEXT' => 'reset it',

    // Signup page.
    'SIGNUP_PAGE_TITLE' => 'Signup',
    'SIGNUP_PAGE_TEXT' => 'Please fill out the following fields to signup:',

    // Profile.
    'TITLE_PROFILE' => 'Profile',

    'FORM_CAPTHA' => 'Verification Code',

    // Errors.
    'ERROR_USERNAME_EXISTS' => 'This username has already been taken.',
    'ERROR_EMAIL_EXISTS' => 'This email address has already been taken.',
    'ERROR_WRONG_CURRENT_PASSWORD' => 'Wrong current password',
    'ERROR_WRONG_USERNAME_OR_PASSWORD' => 'Wrong username or password.',
    'ERROR_PROFILE_BLOCKED' => 'Your profile blocked.',
    'ERROR_PROFILE_NOT_ACTIVATE' => 'You need to confirm you registration. Check email.',
    'ERROR_USER_NOT_FOUND_BY_EMAIL' => 'User not found with this email.',

    'USER_ROLE' => 'Role',
];