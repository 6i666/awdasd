<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\modules\user\Module;

$this->title = Module::t('module', 'LOGIN_PAGE_TITLE');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Module::t('module', 'LOGIN_PAGE_TEXT') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    <?=  Module::t('module', 'RESET_PASSWORD_TEXT') ?> <?= Html::a(Module::t('module', 'RESET_PASSWORD_LINK_TEXT'), ['/user/default/password-reset-request']) ?>.
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Module::t('module', 'BUTTON_LOGIN'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
