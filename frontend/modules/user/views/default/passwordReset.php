<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\modules\user\Module;

$this->title = Module::t('module', 'RESET_PASSWORD_PAGE_TITLE');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-password-reset">
    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Module::t('module', 'RESET_PASSWORD_PAGE_TEXT') ?></p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'password-reset-form']); ?>

                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton(Module::t('module', 'BUTTON_SAVE'), ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
