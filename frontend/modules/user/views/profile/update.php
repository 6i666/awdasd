<?php

/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\modules\user\Module;
?>

<?php $form = ActiveForm::begin([]); ?>

<?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>

<div class="form-group">
    <?= Html::submitButton(Module::t('module', 'BUTTON_SAVE'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>