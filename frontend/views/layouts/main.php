<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\modules\admin\Module;
use frontend\modules\admin\rbac\Rbac as AdminRbac;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => Yii::t('app', 'NAV_HOME'), 'url' => ['/main/default/index']],
        ['label' => Yii::t('app', 'NAV_ABOUT'), 'url' => ['/main/default/about']],
        ['label' => Yii::t('app', 'NAV_CONTACT'), 'url' => ['/main/contact/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('app', 'NAV_SIGNUP'), 'url' => ['/user/default/signup']];
        $menuItems[] = ['label' => Yii::t('app', 'NAV_LOGIN'), 'url' => ['/user/default/login']];
    } else {
        $menuItems[] = ['label' => Yii::t('app', 'NAV_PROFILE'), 'items' => [
            ['label' => Yii::t('app', 'NAV_PROFILE'), 'url' => ['/user/profile/index']],
            '<li>'
                . Html::beginForm(['/user/default/logout'], 'post')
                . Html::submitButton(
                    Yii::t('app', 'NAV_LOGOUT') . ' (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>',
        ]];
        if (Yii::$app->user->can(AdminRbac::PERMISSION_ADMIN_PANEL)) {
            $menuItems[] = ['label' => Module::t('module', 'NAV_ADMIN'), 'items' => [
                ['label' => Module::t('module', 'NAV_ADMIN'), 'url' => ['/admin/default/index']],
                ['label' => Module::t('module', 'ADMIN_USERS'), 'url' => ['/admin/users/index']],
            ]];
        }
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Yii::$app->name ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
