<?php
return [
    /* Navigations. */
    'NAV_HOME' => 'Home',
    'NAV_ABOUT' => 'About',
    'NAV_CONTACT' => 'Contact',
    'NAV_SIGNUP' => 'Signup',
    'NAV_LOGIN' => 'Login',
    'NAV_LOGOUT' => 'Logout',
    'NAV_PROFILE' => 'Профиль',

    /* Buttons. */
    'BUTTON_SAVE' => 'Save',
    'BUTTON_SEND' => 'Send',
    'BUTTON_UPDATE' => 'Update',
    'BUTTON_DELETE' => 'Delete',
    'BUTTON_LOGIN' => 'Login',

    /* Forms. */
    // Contact form.
    'CONTACT_FORM_NAME' => 'Your name',
    'CONTACT_FORM_SUBJECT' => 'Subject',
    'CONTACT_FORM_BODY' => 'Message',
    'FORM_CAPTHA' => 'Verification Code',

    /* Pages. */
    // About page.
    'TITLE_ABOUT' => 'About',
    // Contact page.
    'TITLE_CONTACT' => 'Contact',
    'CONTACT_PAGE_TEXT' => 'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.',
];