<?php
return [
    /* Navigations. */
    'NAV_HOME' => 'Главная',
    'NAV_ABOUT' => 'О сайте',
    'NAV_CONTACT' => 'Контакты',
    'NAV_SIGNUP' => 'Регистрация',
    'NAV_LOGIN' => 'Войти',
    'NAV_LOGOUT' => 'Выйти',
    'NAV_PROFILE' => 'Профиль',

    /* Buttons. */
    'BUTTON_SAVE' => 'Сохранить',
    'BUTTON_SEND' => 'Отправить',
    'BUTTON_UPDATE' => 'Обновить',
    'BUTTON_DELETE' => 'Удалить',
    'BUTTON_LOGIN' => 'Войти',

    /* Forms. */
    // Contact form.
    'CONTACT_FORM_NAME' => 'Ваше имя',
    'CONTACT_FORM_SUBJECT' => 'Тема',
    'CONTACT_FORM_BODY' => 'Сообщение',
    'FORM_CAPTHA' => 'Введите код с картинки',

    /* Pages. */
    // About page.
    'TITLE_ABOUT' => 'О сайте',
    // Contact page.
    'TITLE_CONTACT' => 'Контакты',
    'CONTACT_PAGE_TEXT' => 'Если у Вас возникли вопросы, пожалуйста свяжитесь со мной.',
    // Login page.
];