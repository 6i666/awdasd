<?php

use yii\db\Migration;

class m170219_192359_add_email_confirm_token_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'email_confirm_token', $this->string()->after('email'));
    }

    public function down()
    {
        echo "m170219_192359_add_email_confirm_token_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
